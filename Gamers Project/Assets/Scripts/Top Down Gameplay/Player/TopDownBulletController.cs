﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownBulletController : MonoBehaviour {
	//Variables
	public float bulletSpeed;

	//Start/Update
	void Start () {
		
	}
	void Update () {
		transform.Translate (Vector3.forward * bulletSpeed * Time.deltaTime);
	}
}
