﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownWeaponController : MonoBehaviour {
	//Variables
	public bool isAttacking;
	public float bulletSpeed;
	public float timeBetweenShots;
	private float shotCounter;

	//Transforms
	public Transform firePoint;

	//GameObjects

	//Subscripts
	public TopDownBulletController bullet;

	//Start/Update
	void Start () {
		
	}
	void Update () {
		if (isAttacking) 
		{
			shotCounter -= Time.deltaTime;
			if (shotCounter <= 0) 
			{
				shotCounter = timeBetweenShots;
				TopDownBulletController newBullet = Instantiate (bullet, firePoint.position, firePoint.rotation) as TopDownBulletController;
				newBullet.bulletSpeed = bulletSpeed;
			}
		} 
		else 
		{
			shotCounter = 0;
		}
	}
}
