﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownPlayerController : MonoBehaviour {
	//Variables
	public float moveSpeed;
	private Rigidbody myRigidbody;

	//Vectors
	private Vector3 moveInput;
	private Vector3 moveVelocity;

	//GameObjects
	private Camera mainCamera;

	//SubScripts
	public TopDownWeaponController theWeapon;

	//Start/Updates
	void Start () {
		myRigidbody = GetComponent<Rigidbody> ();
		mainCamera = FindObjectOfType<Camera> ();
	}
	void Update () {
		moveInput = new Vector3 (Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
		moveVelocity = moveInput * moveSpeed;

		Ray cameraRay = mainCamera.ScreenPointToRay (Input.mousePosition);
		Plane groundPlane = new Plane (Vector3.up, Vector3.zero);
		float rayLength;

		if(groundPlane.Raycast(cameraRay, out rayLength))
		{
			Vector3 pointToLook = cameraRay.GetPoint (rayLength);
			Debug.DrawLine (cameraRay.origin, pointToLook, Color.blue);

			transform.LookAt (new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
		}
		if(Input.GetMouseButtonDown(0))
		{
			theWeapon.isAttacking = true;
		}
		if(Input.GetMouseButtonUp(0))
		{
			theWeapon.isAttacking = false;
		}
	}
	void FixedUpdate(){
		myRigidbody.velocity = moveVelocity;
	}

	//Methods
}
